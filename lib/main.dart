import 'dart:async';

import 'package:catcher/core/catcher.dart';
import 'package:catcher/handlers/console_handler.dart';
import 'package:catcher/handlers/email_manual_handler.dart';
import 'package:catcher/handlers/toast_handler.dart';
import 'package:catcher/mode/dialog_report_mode.dart';
import 'package:catcher/model/catcher_options.dart';
import 'package:catcher/model/toast_handler_gravity.dart';
import 'package:catcher/model/toast_handler_length.dart';
import 'package:fimber/fimber_base.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:taskit/responsive_utils.dart';
import 'package:taskit/themes_colors.dart';
import 'package:taskit/themes_textstyles.dart';
import 'package:taskit/themes_widgets.dart';
import 'package:taskit/ui/home.dart';

import 'dependencies_injecting.dart';

// ignore: prefer_void_to_null
Future<Null> mainDelegate() async{

  // get connection, are we internet connected/


  // This captures errors reported by the Flutter framework.
  FlutterError.onError = (FlutterErrorDetails details) async {
    if (myAppInjector.get<AppDebugMode>().isInDebugMode) {
      // In development mode simply print to console.
      FlutterError.dumpErrorToConsole(details);
    } else {
      // In production mode report to the application zone to report to
      // Sentry.
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  if (myAppInjector.get<AppDebugMode>().isInDebugMode) {

  } else {
    // not in docs but in the fimber class itself at
    // https://github.com/magillus/flutter-fimber/blob/master/fimber/lib/fimber.dart
    final List<String> myLogLevels = ["D", "I", "W", "E"];
    Fimber.plantTree(DebugTree(useColors: true, logLevels:  myLogLevels, printTimeType: 1));
  }
  // per this so answer https://stackoverflow.com/questions/57879455/flutter-catching-all-unhandled-exceptions
  await runZoned<Future<void>>(
          () async {

        final CatcherOptions debugOptions =
        CatcherOptions(DialogReportMode(), [ConsoleHandler()]);
        final CatcherOptions releaseOptions = CatcherOptions(DialogReportMode(), [
          EmailManualHandler([myAppInjector.get<AppCrashReportAddy>().emailAddy])
        ]);
        CatcherOptions(DialogReportMode(), [
          EmailManualHandler([myAppInjector.get<AppCrashReportAddy>().emailAddy],
              enableDeviceParameters: true,
              enableStackTrace: true,
              enableCustomParameters: true,
              enableApplicationParameters: true,
              sendHtml: true,
              emailTitle: (myAppInjector.get<AppName>().myAppName) + " Exceptions",
              emailHeader: "Exceptions",
              printLogs: true)
        ]);
        CatcherOptions(DialogReportMode(), [
          ToastHandler(
              gravity: ToastHandlerGravity.bottom,
              length: ToastHandlerLength.long,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              textSize: 12.0,
              customMessage: "We are sorry but unexpected error occured.")
        ]);
        CatcherOptions(DialogReportMode(), [ConsoleHandler(enableApplicationParameters: true,
            enableDeviceParameters: true,
            enableCustomParameters: true,
            enableStackTrace: true)]);


        Catcher(Main(), debugConfig: debugOptions, releaseConfig: releaseOptions);


      }, onError: (dynamic error, dynamic stackTrace) {
    Catcher.reportCheckedError(error, stackTrace);
  });

}

class Main extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) => App();
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}
class _AppState extends State<App> {






  Brightness brightness = Brightness.light;

  @override
  Widget build(BuildContext context) {

    // hmm do I ned anootated region?
    // seems to work but eventually should move to using
    // annotedregion as I have nested widgets and diff appbars
    // per platforms
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values) ;
    // this for full screen
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
        systemNavigationBarIconBrightness: Brightness.light
    ));


    // need to add appbar themes for both
    // materialapp and cupertinoapp
    // as of 1-30-2020 still have to set
    // fontFamily in material theme for fonts
    // on all platforms ie no set font for iios separate
    // from android
    final materialTheme = ThemeData(
      // as of 2-7-2020 I still need to set this for appbar color
      primaryColor: Colors.purple,

      colorScheme: myLightColorScheme,
      textTheme: myMaterialTextTheme,
      buttonTheme: myButtonTheme,







    );
    final materialDarkTheme = ThemeData(
      brightness: Brightness.dark,


      colorScheme: myDarkColorScheme,
      textTheme: myMaterialTextTheme,


    );

    final cupertinoTheme =
    CupertinoThemeData(
      // work-around current CuperinoApp using defualt roboto fonts
      // and google_fonts pluginnot realy ready for beta
      // other part is defining fonts in pubspec
      // backout not ready yet
      // still cannot set fonts 1-30-2020 when using the
      // flutter_platform_widgets plugin but not usre if its that
      //plugin's falut
      //textTheme: const CupertinoTextThemeData(
      //textStyle: TextStyle(fontFamily: 'WorkSans')
      //),



        brightness: brightness, // if null will use the system theme
        primaryColor: primaryPurple,



        barBackgroundColor: barBackgroundPrimary,
        textTheme: myCupertinoTextTheme



    );

    // Example of optionally setting the platform upfront.
    //final initialPlatform = TargetPlatform.iOS;

    // If you mix material and cupertino widgets together then you cam
    // set this setting. Will mean ios darmk mode to not to work properly
    //final settings = PlatformSettingsData(iosUsesMaterialWidgets: true);

    // This theme is required since icons light/dark mode will look for it
    return Theme(
      data: brightness == Brightness.light ? materialTheme : materialDarkTheme,
      child: PlatformProvider(
        //initialPlatform: initialPlatform,
        builder: (context) => PlatformApp(
          // so I can do vid recording of debug runs without the banner
          debugShowCheckedModeBanner: false,
          navigatorKey: Catcher.navigatorKey,
          localizationsDelegates: <LocalizationsDelegate<dynamic>>[
            DefaultMaterialLocalizations.delegate,
            DefaultWidgetsLocalizations.delegate,
            DefaultCupertinoLocalizations.delegate,
          ],
          title: 'Flutter Platform Widgets',
          android: (_) {
            return MaterialAppData(
              theme: materialTheme,
              darkTheme: materialDarkTheme,
              themeMode: brightness == Brightness.light
                  ? ThemeMode.light
                  : ThemeMode.dark,
            );
          },
          ios: (_) => CupertinoAppData(
            theme: cupertinoTheme,
          ),
          builder:(BuildContext context, Widget widget) {
            Catcher.addDefaultErrorWidget(
                showStacktrace: true,
                customTitle: "Custom error title",
                customDescription: "Custom error description");
            return widget;
          },
          home: LandingPage(() {
            setState(() {
              brightness = brightness == Brightness.light
                  ? Brightness.dark
                  : Brightness.light;
            });
          }),
        ),
      ),
    );
  }



}

class LandingPage extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  LandingPage(this.toggleBrightness);

  final void Function() toggleBrightness;

  @override
  LandingPageState createState() => LandingPageState();
}
class LandingPageState extends State<LandingPage> {
  @override
  // ignore: type_annotate_public_apis, always_declare_return_types
  initState() {
    super.initState();
  }




  @override
  Widget build(BuildContext context) {
    // than I can call the proper
    // ResponsiveConfig.sizing in
    // my ui containers including correct font sizing

    ResponsiveConfig.init(context, allowFontScaling: true);

    // need to switch to
    // stack
    //   container
    // box decoration
    // content below appbar
    // positioned widget
    // appBar is child of the positioned widget
    // above this set body to new widget defintion
    // and have new widget def do what I need to do
    return PlatformScaffold(

      iosContentPadding: true,
      appBar: PlatformAppBar(
        title: const Text('Flutter Boilerplate',),
        //backgroundColor: Colors.transparent,


        android: (_) => myMaterialAppBarData,


        // inmy litview pages need to set transparency
        ios: (_) => myCupertinoNavigationBarData,
        trailingActions: <Widget>[
          PlatformIconButton(
            padding: EdgeInsets.zero,
            iosIcon: Icon(CupertinoIcons.share),
            androidIcon: Icon(Icons.share),

            ios: (_) => myCupertinoIconButtonData,
            android: (_) => myMaterialIconButtonData,
            onPressed: () {},
          ),
        ],
      ),

      body: HomePage(title: 'Taskit'),


    );
  }

}
