import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:taskit/models/task.dart';
import 'package:flutter/cupertino.dart';

class NewTaskPage extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  NewTaskPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _NewTaskPageState();
}

class _NewTaskPageState extends State<NewTaskPage> {
  TextEditingController _titleController, _descriptionController;

  int maxTitleLength = 24;
  bool _isSaveButtonVisible = false;

  void _saveTaskAndClose() {
    final String title = _titleController.text;
    final String description = _descriptionController.text;

    if (title.trim().isEmpty) {
      return;
    }

    Navigator.pop(context, Task(0, title, description, false));
  }

  @override
  void initState() {
    super.initState();
    _descriptionController = TextEditingController(text: '');
    _titleController = TextEditingController(text: '');
  }

  @override
  void dispose() {
    _titleController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final saveButton = PlatformIconButton(
        onPressed: _saveTaskAndClose,

        icon: Icon(
          Icons.save,
          size: 32,
          color: Theme.of(context).primaryColor,
        ));
    return PlatformScaffold(
        body: Material(
          color: Colors.white,
          child: Stack(
            children: <Widget>[
              ListView(
                shrinkWrap: false,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Row(
                      children: <Widget>[
                        PlatformIconButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            icon: Icon(
                              Icons.arrow_back,
                              size: 32,
                              color: Colors.grey,
                            )),
                        const Spacer(),
                        if (_isSaveButtonVisible) saveButton else const Spacer(),
                      ],
                    ),
                  ),
                  PlatformTextField(
                    maxLength: maxTitleLength,
                    controller: _titleController,
                    style: TextStyle(
                      fontSize: 24.0,
                      color: Colors.black,
                    ),

                    onChanged: (text) {
                      final needSaveButton =
                          _titleController.text.trim().isNotEmpty;
                      if (_isSaveButtonVisible != needSaveButton) {
                        setState(() {
                          _isSaveButtonVisible = needSaveButton;
                        });
                      }
                    },
                  ),
                  PlatformTextField(
                    controller: _descriptionController,
                    keyboardType: TextInputType.multiline,
                    maxLines: 10,
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.black,
                    ),

                  ),
                ],
              )
            ],
          ),
        ));
  }
}
