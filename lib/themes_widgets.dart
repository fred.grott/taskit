// BSD Clause 2 Copyright Fredrick Allan  Grott 2020



import 'package:flutter/material.dart';
import 'package:taskit/themes_colors.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

final ButtonThemeData myButtonTheme = ButtonThemeData(
  colorScheme: myLightColorScheme,
  splashColor: Colors.deepPurpleAccent[100],



);

// Just mapping out
// how to imporve the flutter platform widgets code exampel and docs

/*
   just right from the class from flutter platform widgets plugin
   https://github.com/aqwert/flutter_platform_widgets/blob/master/lib/src/platform_button.dart
 */
final MaterialRaisedButtonData myMaterialRaisedButtonData = MaterialRaisedButtonData(


  color: Colors.purple,







);

final MaterialFlatButtonData myMaterialFlatButtonData = MaterialFlatButtonData(


  color: Colors.purple,





);

/*
  from the same flutter platform widgets class
  https://github.com/aqwert/flutter_platform_widgets/blob/master/lib/src/platform_button.dart
 */
final CupertinoButtonData myCupertinoButtonData = CupertinoButtonData(


  color: primaryPurple,








);

final MaterialSwitchData myMaterialSwitchData = MaterialSwitchData(

  activeColor: Colors.purple,

);

final CupertinoSwitchData myCupertinoSwitchData = CupertinoSwitchData(
    activeColor: Colors.purple

);

final MaterialSliderData myMaterialSliderData = MaterialSliderData(

    activeColor: Colors.purple
);

final CupertinoSliderData myCupertinoSliderData = CupertinoSliderData(

    activeColor: Colors.purple
);

final MaterialIconButtonData myMaterialIconButtonData = MaterialIconButtonData(


);

final CupertinoIconButtonData myCupertinoIconButtonData = CupertinoIconButtonData(


  color: iconButtonPrimaryColor,




);

final MaterialProgressIndicatorData myMaterialProgressIndicatorData = MaterialProgressIndicatorData(
    backgroundColor: Colors.black12

);

final CupertinoProgressIndicatorData myCupertinoProgressIndicatorData = CupertinoProgressIndicatorData(


);

// MaterialAppbarData and CupertinoNvigationBarData have in common
// automaticallyImplLeading(bool)
// backgroundColor(Color)
// herotag(Object) is in materialappbar by default and added to cupertinoNavigationBarData
// leading(widegt)
// title(widget)
// widgetkey
//
// so take aways is
// 1. I can have a non primary appbar color such as the primary variant
// 2. hero transitions involving the appbar
// 3. leading logo or icon as its own widget with possible hero animation
// 4. define a title thatis more stylistic and has its own hero stuff


final MaterialAppBarData myMaterialAppBarData = MaterialAppBarData(







);
final CupertinoNavigationBarData myCupertinoNavigationBarData = CupertinoNavigationBarData(






);

// MaterialnavBarData and CupertinoTabBarData have in common
// currentIndex
// backgroundColor
// itemChanged
// items
// itemSize
// widgetKey



final MaterialNavBarData myMaterialNavBarData = MaterialNavBarData(


);

final CupertinoTabBarData myCupertinotabBarData = CupertinoTabBarData(



);

// MaterialAlertDialogData and CupertinoAlertDialogData
// have in comoon
// actions
// content
// title
// widgetKey

final MaterialAlertDialogData myMaterialAlertDialogData = MaterialAlertDialogData(




);

final CupertinoAlertDialogData myCupertinoAlertDialogData = CupertinoAlertDialogData(






);

// MaterialdialogActionData and CupertinoDialogActionData
// have in common
// widgetKey
// textTheme for material and textStyle for cupertino

final MaterialDialogActionData myMaterialDialogActionData = MaterialDialogActionData(



);

final CupertinoDialogActionData myCupertinoDialogActionData = CupertinoDialogActionData(



);

// MaterialTabScaffolddata and CupertinoTabScaffoldData have in common
// widgetKet
// backgroundColor
// appBarBuilder
// bodyBuilder
// controller
// resizeToAvoidBottomInset
// tabBackgroundColor

final MaterialTabScaffoldData myMaterialTabScaffoldDate = MaterialTabScaffoldData(



);

final CupertinoTabScaffoldData myCupertinoTabScaffoldData = CupertinoTabScaffoldData(




);

// MaterialScaffoldData and CupertinoPageScaffoldData in common
// widgetKey
// backgroundColor
// resizeToAvoidBottomInset
// bottomNavBar bottomTabBar

final MaterialScaffoldData myMaterialScaffoldData = MaterialScaffoldData(



);

final CupertinoPageScaffoldData myCupertinoPageScaffoldData = CupertinoPageScaffoldData(



);

// MatrialTextFielddatta and CupertinoTextFiledDat have in common
// controller
// widgetKey
// focusNode
// autofocus
// scrollController
// autocorrect
// cursorColor
// cursorRadius
// cursorWidth
// decoration
// dragStartBehaviior
// enabled
// enableInteractiveSelection
// enableSuggestions
// expand
// inputFormatters
// keyboardAppearance
// keyboardType
// maxLength
// maxLengthEnforced
// maxLines
// minLines
// obscureText
// onchanged
// onEditingComplete
// onSubmitted
// onTap
// readOnly
// scrollPadding
// scrollPhysics
// strutStyle
// style
// textAlign
// textAlignVertical
// textCapitalization
// textInputAction
// toolbarOptions


final MaterialTextFieldData myMaterialTextFieldData = MaterialTextFieldData(





);

final CupertinoTextFieldData myCupertinoTextFieldData = CupertinoTextFieldData(



);

