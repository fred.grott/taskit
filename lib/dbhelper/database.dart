import 'dart:async';
import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:taskit/models/task.dart';

class DatabaseUtil {
  DatabaseUtil._();

  static final DatabaseUtil db = DatabaseUtil._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }

    // ignore: join_return_with_assignment, unnecessary_parenthesis
    _database = (await init()) as Database;
    return _database;
  }

  dynamic init() async {
    final Directory documentDirectory = await getApplicationDocumentsDirectory();
    final String path = join(documentDirectory.path, 'tasks.db');

    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE Task ('
          'id INTEGER PRIMARY KEY,'
          'title TEXT,'
          'description TEXT,'
          'done INTEGER,'
          'pomCount INTEGER'
          ')');
    });
  }

  dynamic insert(Task task) async {
    final db = await database;

    final table = await db.rawQuery('SELECT MAX(id)+1 as id FROM Task');
    final dynamic id = table.first['id'];

    final raw = await db.rawInsert(
        'INSERT Into Task (id, title, description, done, pomCount) VALUES (?,?,?,?,?)',
        <dynamic>[id, task.title, task.description, if (task.done) 1 else 0, task.pomCount]);

    print('Saved: $raw');
    return raw;
  }

  dynamic update(Task task) async {
    final db = await database;

    final int raw = await db.rawUpdate(
        'UPDATE Task SET title = ?, description = ?, done = ?, pomCount = ? WHERE id = ?',
        <dynamic>[
          task.title,
          task.description,
          if (task.done) 1 else 0,
          task.pomCount,
          task.id
        ]);

    print('Updated');
    return raw;
  }

  dynamic remove(Task task) async {
    final db = await database;

    final raw = await db.rawUpdate('DELETE FROM Task WHERE id = ?', <dynamic>[task.id]);

    print('Removed');
    return raw;
  }

  Future<List<Task>> getAll() async {
    final db = await database;
    final query = await db.query('Task');

    final List<Task> tasks =
        query.isNotEmpty ? query.map((t) => Task.fromMap(t)).toList() : [];

    return tasks;
  }
}
