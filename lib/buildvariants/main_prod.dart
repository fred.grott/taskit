import 'package:taskit/main.dart';



import '../dependencies_injecting.dart';




void main(){

  myAppInjector.get<AppBuildVariants>().name("prod");
  myAppInjector.get<AppEndPoint>().appEndPoint("prod.me.me");


  mainDelegate();
}